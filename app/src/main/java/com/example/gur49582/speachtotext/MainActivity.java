package com.example.gur49582.speachtotext;


import android.content.ActivityNotFoundException;
import android.content.Intent;

import com.android.volley.RequestQueue;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.ArrayList;


import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity implements TextToSpeech.OnInitListener {

    private static final int REQ_CODE_SPEECH_INPUT = 100;
    private TextView mVoiceInputTv;
    private ImageButton mSpeakBtn;
    private TextToSpeech tts;
    private String textToSpeak = "";
    private  String speachConvertedText = "";
    private  ArrayList<String> keywordList;
    private RequestQueue mRequestQueue;
    private StringRequest mStringRequest;
    RequestQueue requestQueue;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        requestQueue = RequestQueueSingelton.getInstance(this.getApplicationContext())
                .getRequestQueue();

        tts = new TextToSpeech(this, this);
        tts.setLanguage(Locale.FRENCH);
        tts.setSpeechRate(1);
        mVoiceInputTv = (TextView) findViewById(R.id.voiceInput);
        mSpeakBtn = (ImageButton) findViewById(R.id.btnSpeak);
        mSpeakBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                startVoiceInput();
            }
        });
    }

    @Override
    public void onInit(int status) {

        if (status == TextToSpeech.SUCCESS) {

            int result = tts.setLanguage(Locale.US);

            if (result == TextToSpeech.LANG_MISSING_DATA
                    || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                Log.e("TTS", "This Language is not supported");
            } else {
                speakOut();
            }

        } else {
            Log.e("TTS", "Initialization Failed!");
        }

    }

    private void speakOut() {
        tts.speak(textToSpeak, TextToSpeech.QUEUE_FLUSH, null);
}




    private void startVoiceInput() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Give Your Input For Maverick");
        try {
            startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);
        } catch (ActivityNotFoundException a) {

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQ_CODE_SPEECH_INPUT: {
                if (resultCode == RESULT_OK && null != data) {
                    ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    speachConvertedText = result.get(0);
                    mVoiceInputTv.setText(speachConvertedText);
                    keywordList = extractKeywordFromText(speachConvertedText);
                    textToSpeak = CallTestOrDevelopBasedOnKeywords(keywordList);
                }
                break;
            }

        }
    }
    private String getspeachConvertedText(){
            return  this.speachConvertedText;

    }




    private String CallTestOrDevelopBasedOnKeywords(ArrayList<String> keywordList){
        String modifiedStatus = null;
        boolean isTest = false;
        boolean isDevelop = false;
        String response = null;
        isDevelop = searchForDevelopKeywords(keywordList);
        isTest = searchForTestKeywords(keywordList);

        if((isTest && isDevelop) || (!isTest && !isDevelop)){
            textToSpeak = "Unable to Identify Between Develop or test." +
                    "Your language is So confusing " +
                    "Please Clearly specify whether you want to Test or Develop?";
            speakOut();
        }
        if(isTest && !isDevelop){
            textToSpeak =  "Running Your Test Suite or Test Case";
            speakOut();
            response = sendRequestToRunTest(keywordList);
        }
        if(isDevelop && !isTest){
            textToSpeak =  "Developing Your Requirements Please have patience";
            speakOut();
            response = sendRequestToDevelop(keywordList);
        }
        return  response;
    }

    private String sendRequestToRunTest(ArrayList<String> keywordList){
    String response = null;

        JSONObject json = new JSONObject();
        try {
            json.put("store","amz");
            json.put("category","fashion");
            json.put("type","coupons");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String url = "www.google.com";
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, json,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        mVoiceInputTv.setText("String Response : "+ response.toString());
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mVoiceInputTv.setText("Error getting response");
            }
        });
        requestQueue.add(jsonObjectRequest);
        return  response;



    }

    private String sendRequestToDevelop(ArrayList<String> keywordList){
        String response = null;


        return  response;
    }


    @Override
    public void onDestroy() {
        if (tts != null) {
            tts.stop();
            tts.shutdown();
        }
        super.onDestroy();
    }

    private boolean searchForDevelopKeywords(ArrayList<String> keywordList){
        boolean isDevelop = false;
        for(String developKeyword:ConstantUtil.DEVELOP_KEYWORDS){
            for(String keyword: keywordList){
                 if(keyword.toLowerCase().indexOf(developKeyword)>= 0){
                     isDevelop = true;
                 }
            }

        }
        return isDevelop;
    }


    private boolean searchForTestKeywords(ArrayList<String> keywordList){
        boolean isDevelop = false;
        for(String testKeyword:ConstantUtil.TEST_KEYWORDS){
            for(String keyword: keywordList){
                if(keyword.toLowerCase().indexOf(testKeyword)>= 0){
                    isDevelop = true;
                }
            }

        }
        return isDevelop;
    }




    public   ArrayList<String> extractKeywordFromText(String text){
        ArrayList<String> list=new ArrayList<String>();
        for(String keyword:ConstantUtil.HEADER_ARRAY){
            if(text.toLowerCase().indexOf(keyword)>= 0){
                list.add(keyword);
            }
        }
        for(String keyword:ConstantUtil.DEVELOP_KEYWORDS){
            if(text.toLowerCase().indexOf(keyword)>= 0){
                list.add(keyword);
            }
        }
        for(String keyword:ConstantUtil.TEST_KEYWORDS){
            if(text.toLowerCase().indexOf(keyword)>= 0){
                list.add(keyword);
            }
        }

        return list;
    }

    public String send(String  url)
    {
        String result;
        try {
        HttpGetRequest getRequest = new HttpGetRequest();
        result = getRequest.execute(url).get();
            } catch (Exception e) {
            result = "\n Sending Server Request...\nFailed Request Due to Exception \n" + e;
            }
            return   result;
    }

}