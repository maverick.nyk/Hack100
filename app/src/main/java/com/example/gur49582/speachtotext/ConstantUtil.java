package com.example.gur49582.speachtotext;

public class ConstantUtil {

    private ConstantUtil(){

    }

    public static final String[] HEADER_ARRAY = {
            "form",
            "button",
            "textfields",
            "submit button"
    };
    public  static  final String[] DEVELOP_KEYWORDS= {
            "develop",
            "create"
    };

    public static final String[] TEST_KEYWORDS = {
            "test",
            "verify",
            "run"
    };
}

